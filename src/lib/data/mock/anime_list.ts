export const anime_list = [
  {
    "id": 1,
    "titles": {
      "japanese": "氷菓",
      "eng": "Hyouka",
      "others": "Hyou-ka, Hyouka: Hyoka",
    },
    basic_details: {
      "type": "TV",
      "episodes": 22,
      "status": "Completed",
      "premiered": "Spring 2012",
      "studio": "Kyoto Animation",
    },
    "score": 79,
    "rating": 4.59,
    "totalResponse": 3058,
    "milestones": [
      { "title": "Most popular of all time", "value": 86 },
      { "title": "Highest rated all time", "value": 260 }
    ],
    "description": `High school freshman Houtarou Oreki has but one goal: to lead a gray life while conserving as much energy as he can. Unfortunately, his peaceful days come to an end when his older sister, Tomoe, forces him to save the memberless Classics Club from disbandment.

    Luckily, Orekis predicament seems to be over when he heads to the clubroom and discovers that his fellow first-year, Eru Chitanda, has already become a member. However, despite his obligation being fulfilled, Oreki finds himself entangled by Chitandas curious and bubbly personality, soon joining the club of his own volition. Hyouka follows the four members of the Classics Club—including Orekis friends Satoshi Fukube and Ibara—as , driven by Chitandas insatiable curiosity, solve the trivial yet intriguing mysteries that permeate their daily lives.`,
    "generes": [
      "Mystery", "Slice of Life"
    ],
    "cardBackgroundImage": "https://cdn.myanimelist.net/images/anime/13/50521.jpg",
    "bannerBackgroundImage": "https://raw.githubusercontent.com/baseplate-admin/CoreProject/19a84ff9c864e0cfb33cb6a4ce79f75af05cde6c/frontend/AnimeCore/static/images/Hyouka-poster.jpg"
  },
]